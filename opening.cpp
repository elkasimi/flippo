#include "Board.h"
#include "Helpers.h"
#include "MCTS.h"

#include <algorithm>
#include <cstdio>
#include <future>
#include <map>
#include <vector>

using namespace std;

void find_best_move(const vector<int>& moves) {
  Logger::level = Logger::ERROR;

  Board board{};
  for (int move : moves) {
    board.do_move(move);
  }

  vector<pair<double, int>> weighted_moves;
  auto run_game = [&board](int move) -> double {
    auto max_total_time = 500;
    auto max_iterations = 1000;
    auto b = board;
    b.do_move(move);
    MCTS ai{b};
    ai.set_max_total_time(max_total_time);
    ai.set_max_iterations(max_iterations);
    ai.disable_opening_book();

    while (not b.end_game()) {
      ai.play_best_move();
    }
    return board.last_moved() == BLACK ? b.score() : -b.score();
  };

  for (int move : board) {
    auto value = 0.0;
    auto count = 204;
    auto cpu_count = 12;
    for (int i = 0; i < count; i += cpu_count) {
      printf("\b\b\b\b%.0f%%", 100.0 * i / count);
      fflush(stdout);
      vector<future<double>> scores;
      for (int j = 0; j < cpu_count; ++j) {
        scores.emplace_back(async(launch::async, run_game, move));
      }
      for (auto& f : scores) {
        value += f.get();
      }
    }
    printf("\b\b\b");
    fflush(stdout);
    value /= count;
    weighted_moves.emplace_back(value, move);
  }
  auto iterator = max_element(weighted_moves.begin(), weighted_moves.end());
  printf("{{");
  for (int move : moves) {
    printf("%d,", move);
  }
  printf("}");
  printf(", %d},//%.3f\n", iterator->second, iterator->first);
}

namespace {
map<vector<int>, int> OPENING_DATA{
    // 1st
    {{}, 20},
    // 2nd
    {{
         18,
     },
     43},
    {{
         20,
     },
     19},
    {{
         29,
     },
     21},
    {{
         34,
     },
     26},
    {{
         43,
     },
     26},
    {{
         45,
     },
     37},
    // 3rd
    {{
         20, 19,
     },
     34},  // 0.569
    {{
         20, 21,
     },
     18},  // 0.520
    {{
         20, 37,
     },
     45},  // 0.402
    // 4th
    {{
         18, 43, 9,
     },
     0},  // 0.569
    {{
         18, 43, 19,
     },
     11},  // 0.137
    {{
         18, 43, 20,
     },
     51},  // 0.078
    {{
         18, 43, 21,
     },
     9},  // 0.275
    {{
         18, 43, 34,
     },
     29},  // 0.216
    {{
         18, 43, 37,
     },
     51},  // 0.137
    {{
         18, 43, 45,
     },
     19},  // 0.147
    {{
         18, 43, 50,
     },
     9},  // 0.265
    {{
         18, 43, 51,
     },
     29},  //-0.069
    {{
         20, 19, 10,
     },
     37},  // 0.186
    {{
         20, 19, 12,
     },
     44},  // 0.373
    {{
         20, 19, 18,
     },
     11},  // 0.118
    {{
         20, 19, 26,
     },
     37},  //-0.039
    {{
         20, 19, 34,
     },
     21},  //-0.196
    {{
         20, 19, 42,
     },
     43},  // 0.147
    {{
         20, 19, 44,
     },
     29},  // 0.069
    {{
         29, 21, 13,
     },
     19},  // 0.020
    {{
         29, 21, 18,
     },
     19},  // 0.098
    {{
         29, 21, 20,
     },
     19},  //-0.245
    {{
         29, 21, 22,
     },
     44},  // 0.108
    {{
         29, 21, 26,
     },
     17},  // 0.029
    {{
         29, 21, 30,
     },
     13},  // 0.108
    {{
         29, 21, 34,
     },
     44},  //-0.186
    {{
         29, 21, 43,
     },
     42},  // 0.088
    {{
         29, 21, 45,
     },
     30},  // 0.206
    {{
         34, 26, 17,
     },
     25},  // 0.324
    {{
         34, 26, 18,
     },
     42},  //-0.167
    {{
         34, 26, 19,
     },
     25},  // 0.235
    {{
         34, 26, 20,
     },
     42},  //-0.078
    {{
         34, 26, 21,
     },
     41},  //-0.108
    {{
         34, 26, 33,
     },
     25},  // 0.382
    {{
         34, 26, 37,
     },
     42},  // 0.206
    {{
         43, 26, 17,
     },
     29},  // 0.078
    {{
         43, 26, 18,
     },
     25},  //-0.029
    {{
         43, 26, 19,
     },
     25},  // 0.333
    {{
         43, 26, 20,
     },
     29},  // 0.118
    {{
         43, 26, 21,
     },
     45},  // 0.108
    {{
         43, 26, 29,
     },
     45},  // 0.157
    {{
         43, 26, 34,
     },
     33},  // 0.206
    {{
         43, 26, 37,
     },
     34},  // 0.206
    {{
         43, 26, 50,
     },
     25},  // 0.020
    {{
         43, 26, 51,
     },
     25},  // 0.039
    {{
         45, 37, 18,
     },
     21},  //-0.010
    {{
         45, 37, 20,
     },
     38},  // 0.098
    {{
         45, 37, 29,
     },
     21},  // 0.167
    {{
         45, 37, 34,
     },
     44},  // 0.206
    {{
         45, 37, 38,
     },
     42},  // 0.206
    {{
         45, 37, 43,
     },
     53},  // 0.147
    {{
         45, 37, 54,
     },
     63},  // 1.020
    // 5th
    {{
         20, 19, 34, 21,
     },
     42},  // 0.500
    {{
         20, 19, 34, 37,
     },
     41},  // 0.725
    {{
         20, 19, 34, 43,
     },
     33},  // 0.461
    {{
         20, 21, 18, 9,
     },
     0},  // 1.265
    {{
         20, 21, 18, 12,
     },
     34},  // 0.480
    {{
         20, 21, 18, 13,
     },
     34},  // 0.304
    {{
         20, 21, 18, 14,
     },
     7},  // 1.706
    {{
         20, 21, 18, 19,
     },
     12},  // 0.510
    {{
         20, 21, 18, 26,
     },
     43},  // 0.422
    {{
         20, 21, 18, 29,
     },
     34},  // 0.451
    {{
         20, 21, 18, 37,
     },
     45},  // 0.765
    {{
         20, 21, 18, 42,
     },
     44},  // 0.480
    {{
         20, 21, 18, 43,
     },
     14},  // 0.696
    {{
         20, 21, 18, 44,
     },
     22},  // 0.392
    {{
         20, 21, 18, 45,
     },
     54},  // 0.882
    {{
         20, 37, 45, 19,
     },
     18},  // 0.392
    {{
         20, 37, 45, 21,
     },
     34},  // 0.598
    {{
         20, 37, 45, 34,
     },
     38},  // 1.029
    {{
         20, 37, 45, 38,
     },
     18},  // 0.627
    {{
         20, 37, 45, 53,
     },
     13},  // 0.422
    // 6th
    {{
         18, 43, 9, 0, 19,
     },
     11},  // 1.010
    {{
         18, 43, 9, 0, 20,
     },
     29},  // 1.088
    {{
         18, 43, 9, 0, 21,
     },
     29},  // 1.127
    {{
         18, 43, 9, 0, 29,
     },
     45},  // 0.804
    {{
         18, 43, 9, 0, 34,
     },
     42},  // 1.686
    {{
         18, 43, 9, 0, 37,
     },
     20},  // 1.392
    {{
         18, 43, 9, 0, 45,
     },
     20},  // 1.186
    {{
         18, 43, 9, 0, 50,
     },
     42},  // 0.961
    {{
         18, 43, 9, 0, 51,
     },
     42},  // 1.078
    {{
         18, 43, 19, 11, 4,
     },
     44},  // 0.235
    {{
         18, 43, 19, 11, 9,
     },
     0},  // 1.216
    {{
         18, 43, 19, 11, 20,
     },
     37},  // 0.206
    {{
         18, 43, 19, 11, 34,
     },
     25},  // 0.324
    {{
         18, 43, 19, 11, 45,
     },
     50},  // 0.137
    {{
         18, 43, 19, 11, 50,
     },
     21},  // 0.275
    {{
         18, 43, 20, 51, 9,
     },
     0},  // 1.225
    {{
         18, 43, 20, 51, 12,
     },
     37},  // 0.078
    {{
         18, 43, 20, 51, 19,
     },
     11},  //-0.363
    {{
         18, 43, 20, 51, 26,
     },
     17},  // 0.059
    {{
         18, 43, 20, 51, 29,
     },
     13},  // 0.088
    {{
         18, 43, 20, 51, 34,
     },
     19},  //-0.343
    {{
         18, 43, 20, 51, 42,
     },
     33},  //-0.029
    {{
         18, 43, 20, 51, 44,
     },
     21},  // 0.039
    {{
         18, 43, 20, 51, 45,
     },
     37},  // 0.167
    {{
         18, 43, 20, 51, 50,
     },
     45},  // 0.088
    {{
         18, 43, 20, 51, 59,
     },
     13},  //-0.049
    {{
         18, 43, 21, 9, 0,
     },
     29},  //-1.157
    {{
         18, 43, 21, 9, 14,
     },
     0},  // 1.029
    {{
         18, 43, 21, 9, 19,
     },
     0},  // 0.833
    {{
         18, 43, 21, 9, 20,
     },
     0},  // 0.598
    {{
         18, 43, 21, 9, 26,
     },
     0},  // 0.863
    {{
         18, 43, 21, 9, 34,
     },
     0},  // 0.971
    {{
         18, 43, 21, 9, 37,
     },
     0},  // 0.784
    {{
         18, 43, 21, 9, 42,
     },
     0},  // 0.873
    {{
         18, 43, 21, 9, 44,
     },
     0},  // 1.382
    {{
         18, 43, 21, 9, 50,
     },
     0},  // 1.088
    {{
         18, 43, 21, 9, 51,
     },
     0},  // 0.765
    {{
         18, 43, 34, 29, 20,
     },
     12},  // 0.118
    {{
         18, 43, 34, 29, 26,
     },
     25},  // 0.353
    {{
         18, 43, 34, 29, 30,
     },
     41},  // 0.157
    {{
         18, 43, 34, 29, 37,
     },
     30},  //-0.069
    {{
         18, 43, 34, 29, 42,
     },
     30},  // 0.216
    {{
         18, 43, 34, 29, 44,
     },
     51},  // 0.255
    {{
         18, 43, 34, 29, 45,
     },
     19},  // 0.059
    {{
         18, 43, 34, 29, 52,
     },
     30},  // 0.578
    {{
         18, 43, 37, 51, 19,
     },
     11},  //-0.147
    {{
         18, 43, 37, 51, 29,
     },
     42},  // 0.206
    {{
         18, 43, 37, 51, 34,
     },
     59},  // 0.020
    {{
         18, 43, 37, 51, 45,
     },
     38},  //-0.078
    {{
         18, 43, 37, 51, 59,
     },
     46},  // 0.284
    {{
         18, 43, 45, 19, 9,
     },
     0},  // 0.892
    {{
         18, 43, 45, 19, 20,
     },
     12},  // 0.167
    {{
         18, 43, 45, 19, 54,
     },
     63},  // 0.951
    {{
         18, 43, 50, 9, 0,
     },
     42},  //-1.039
    {{
         18, 43, 50, 9, 19,
     },
     0},  // 0.794
    {{
         18, 43, 50, 9, 20,
     },
     0},  // 0.706
    {{
         18, 43, 50, 9, 21,
     },
     0},  // 1.402
    {{
         18, 43, 50, 9, 29,
     },
     0},  // 0.853
    {{
         18, 43, 50, 9, 34,
     },
     0},  // 0.598
    {{
         18, 43, 50, 9, 37,
     },
     0},  // 0.657
    {{
         18, 43, 50, 9, 51,
     },
     0},  // 0.892
    {{
         18, 43, 50, 9, 57,
     },
     0},  // 0.529
    {{
         18, 43, 51, 29, 9,
     },
     0},  // 0.402
    {{
         18, 43, 51, 29, 19,
     },
     26},  //-0.118
    {{
         18, 43, 51, 29, 20,
     },
     11},  //-0.010
    {{
         18, 43, 51, 29, 21,
     },
     26},  // 0.294
    {{
         18, 43, 51, 29, 22,
     },
     45},  // 0.127
    {{
         18, 43, 51, 29, 26,
     },
     50},  //-0.020
    {{
         18, 43, 51, 29, 30,
     },
     59},  //-0.010
    {{
         18, 43, 51, 29, 34,
     },
     19},  //-0.039
    {{
         18, 43, 51, 29, 37,
     },
     34},  // 0.294
    {{
         18, 43, 51, 29, 42,
     },
     19},  // 0.422
    {{
         18, 43, 51, 29, 44,
     },
     9},  // 0.088
    {{
         18, 43, 51, 29, 45,
     },
     22},  //-0.039
    {{
         18, 43, 51, 29, 50,
     },
     19},  //-0.176
    {{
         18, 43, 51, 29, 59,
     },
     19},  // 0.118
    {{
         20, 19, 10, 37, 1,
     },
     43},  //-0.069
    {{
         20, 19, 10, 37, 12,
     },
     44},  // 0.098
    {{
         20, 19, 10, 37, 18,
     },
     34},  // 0.314
    {{
         20, 19, 10, 37, 21,
     },
     12},  // 0.304
    {{
         20, 19, 10, 37, 26,
     },
     38},  // 0.059
    {{
         20, 19, 10, 37, 34,
     },
     1},  // 0.157
    {{
         20, 19, 10, 37, 42,
     },
     13},  // 0.402
    {{
         20, 19, 10, 37, 43,
     },
     18},  //-0.098
    {{
         20, 19, 10, 37, 44,
     },
     34},  // 0.147
    {{
         20, 19, 10, 37, 46,
     },
     13},  // 0.235
    {{
         20, 19, 12, 44, 4,
     },
     21},  // 0.020
    {{
         20, 19, 12, 44, 10,
     },
     29},  // 0.235
    {{
         20, 19, 12, 44, 26,
     },
     53},  // 0.176
    {{
         20, 19, 12, 44, 42,
     },
     52},  // 0.157
    {{
         20, 19, 12, 44, 52,
     },
     13},  //-0.059
    {{
         20, 19, 18, 11, 2,
     },
     17},  // 0.284
    {{
         20, 19, 18, 11, 4,
     },
     37},  // 0.196
    {{
         20, 19, 18, 11, 9,
     },
     0},  // 0.931
    {{
         20, 19, 18, 11, 10,
     },
     21},  //-0.029
    {{
         20, 19, 18, 11, 12,
     },
     44},  // 0.127
    {{
         20, 19, 18, 11, 17,
     },
     25},  //-0.059
    {{
         20, 19, 18, 11, 21,
     },
     43},  // 0.471
    {{
         20, 19, 18, 11, 26,
     },
     17},  // 0.078
    {{
         20, 19, 18, 11, 34,
     },
     43},  // 0.245
    {{
         20, 19, 18, 11, 42,
     },
     37},  //-0.020
    {{
         20, 19, 18, 11, 44,
     },
     10},  // 0.206
    {{
         20, 19, 18, 11, 45,
     },
     3},  // 0.284
    {{
         20, 19, 26, 37, 11,
     },
     46},  // 0.049
    {{
         20, 19, 26, 37, 12,
     },
     38},  // 0.343
    {{
         20, 19, 26, 37, 13,
     },
     12},  // 0.186
    {{
         20, 19, 26, 37, 18,
     },
     46},  // 0.441
    {{
         20, 19, 26, 37, 25,
     },
     17},  // 0.147
    {{
         20, 19, 26, 37, 29,
     },
     12},  // 0.049
    {{
         20, 19, 26, 37, 34,
     },
     46},  // 0.137
    {{
         20, 19, 26, 37, 43,
     },
     34},  // 0.176
    {{
         20, 19, 26, 37, 44,
     },
     10},  // 0.373
    {{
         20, 19, 26, 37, 45,
     },
     12},  // 0.098
    {{
         20, 19, 34, 21, 10,
     },
     22},  // 0.029
    {{
         20, 19, 34, 21, 11,
     },
     18},  // 0.118
    {{
         20, 19, 34, 21, 12,
     },
     26},  // 0.098
    {{
         20, 19, 34, 21, 13,
     },
     5},  // 0.098
    {{
         20, 19, 34, 21, 14,
     },
     7},  // 0.814
    {{
         20, 19, 34, 21, 18,
     },
     41},  // 0.265
    {{
         20, 19, 34, 21, 26,
     },
     33},  // 0.039
    {{
         20, 19, 34, 21, 29,
     },
     14},  // 0.069
    {{
         20, 19, 34, 21, 33,
     },
     18},  // 0.137
    {{
         20, 19, 34, 21, 37,
     },
     11},  // 0.049
    {{
         20, 19, 34, 21, 41,
     },
     37},  // 0.039
    {{
         20, 19, 34, 21, 42,
     },
     22},  // 0.069
    {{
         20, 19, 34, 21, 43,
     },
     51},  //-0.059
    {{
         20, 19, 34, 21, 44,
     },
     37},  // 0.010
    {{
         20, 19, 34, 21, 45,
     },
     43},  //-0.078
    {{
         20, 19, 42, 43, 10,
     },
     37},  // 0.059
    {{
         20, 19, 42, 43, 11,
     },
     3},  // 0.118
    {{
         20, 19, 42, 43, 12,
     },
     10},  // 0.088
    {{
         20, 19, 42, 43, 13,
     },
     18},  // 0.108
    {{
         20, 19, 42, 43, 18,
     },
     13},  // 0.059
    {{
         20, 19, 42, 43, 21,
     },
     51},  // 0.157
    {{
         20, 19, 42, 43, 26,
     },
     51},  // 0.137
    {{
         20, 19, 42, 43, 29,
     },
     37},  // 0.039
    {{
         20, 19, 42, 43, 34,
     },
     29},  // 0.157
    {{
         20, 19, 42, 43, 44,
     },
     37},  // 0.098
    {{
         20, 19, 42, 43, 45,
     },
     12},  // 0.186
    {{
         20, 19, 42, 43, 49,
     },
     56},  // 0.961
    {{
         20, 19, 42, 43, 50,
     },
     49},  // 0.147
    {{
         20, 19, 42, 43, 51,
     },
     21},  //-0.069
    {{
         20, 19, 44, 29, 10,
     },
     18},  // 0.029
    {{
         20, 19, 44, 29, 12,
     },
     38},  // 0.098
    {{
         20, 19, 44, 29, 18,
     },
     11},  // 0.598
    {{
         20, 19, 44, 29, 26,
     },
     34},  // 0.118
    {{
         20, 19, 44, 29, 30,
     },
     52},  // 0.176
    {{
         20, 19, 44, 29, 34,
     },
     53},  // 0.000
    {{
         20, 19, 44, 29, 38,
     },
     12},  // 0.471
    {{
         20, 19, 44, 29, 42,
     },
     26},  //-0.039
    {{
         20, 19, 44, 29, 52,
     },
     12},  // 0.137
    {{
         29, 21, 13, 19, 5,
     },
     11},  // 0.039
    {{
         29, 21, 13, 19, 18,
     },
     42},  // 0.186
    {{
         29, 21, 13, 19, 20,
     },
     22},  // 0.059
    {{
         29, 21, 13, 19, 22,
     },
     10},  // 0.127
    {{
         29, 21, 13, 19, 26,
     },
     14},  // 0.078
    {{
         29, 21, 13, 19, 34,
     },
     41},  // 0.167
    {{
         29, 21, 13, 19, 37,
     },
     42},  // 0.020
    {{
         29, 21, 13, 19, 42,
     },
     43},  //-0.049
    {{
         29, 21, 13, 19, 43,
     },
     10},  // 0.324
    {{
         29, 21, 18, 19, 9,
     },
     0},  // 1.127
    {{
         29, 21, 18, 19, 11,
     },
     26},  // 0.059
    {{
         29, 21, 18, 19, 13,
     },
     14},  //-0.029
    {{
         29, 21, 18, 19, 20,
     },
     11},  // 0.108
    {{
         29, 21, 18, 19, 22,
     },
     26},  // 0.275
    {{
         29, 21, 18, 19, 26,
     },
     30},  // 0.127
    {{
         29, 21, 18, 19, 30,
     },
     39},  // 0.245
    {{
         29, 21, 18, 19, 34,
     },
     42},  // 0.167
    {{
         29, 21, 18, 19, 43,
     },
     26},  // 0.176
    {{
         29, 21, 18, 19, 45,
     },
     30},  //-0.059
    {{
         29, 21, 20, 19, 10,
     },
     38},  // 0.078
    {{
         29, 21, 20, 19, 11,
     },
     13},  //-0.118
    {{
         29, 21, 20, 19, 12,
     },
     4},  // 0.265
    {{
         29, 21, 20, 19, 13,
     },
     43},  // 0.000
    {{
         29, 21, 20, 19, 14,
     },
     7},  // 0.814
    {{
         29, 21, 20, 19, 18,
     },
     37},  // 0.029
    {{
         29, 21, 20, 19, 22,
     },
     18},  // 0.392
    {{
         29, 21, 20, 19, 26,
     },
     18},  // 0.098
    {{
         29, 21, 20, 19, 30,
     },
     44},  // 0.245
    {{
         29, 21, 20, 19, 34,
     },
     11},  // 0.020
    {{
         29, 21, 20, 19, 42,
     },
     44},  // 0.245
    {{
         29, 21, 20, 19, 43,
     },
     37},  // 0.314
    {{
         29, 21, 20, 19, 44,
     },
     18},  // 0.108
    {{
         29, 21, 22, 44, 20,
     },
     12},  // 0.206
    {{
         29, 21, 22, 44, 30,
     },
     37},  // 0.069
    {{
         29, 21, 22, 44, 43,
     },
     37},  // 0.176
    {{
         29, 21, 22, 44, 45,
     },
     52},  // 0.235
    {{
         29, 21, 26, 17, 13,
     },
     8},  // 0.255
    {{
         29, 21, 26, 17, 14,
     },
     7},  // 0.814
    {{
         29, 21, 26, 17, 18,
     },
     19},  // 0.059
    {{
         29, 21, 26, 17, 20,
     },
     37},  // 0.088
    {{
         29, 21, 26, 17, 22,
     },
     23},  // 0.255
    {{
         29, 21, 26, 17, 25,
     },
     19},  // 0.147
    {{
         29, 21, 26, 17, 30,
     },
     8},  // 0.039
    {{
         29, 21, 26, 17, 34,
     },
     25},  // 0.098
    {{
         29, 21, 26, 17, 42,
     },
     49},  // 0.186
    {{
         29, 21, 26, 17, 43,
     },
     34},  // 0.039
    {{
         29, 21, 26, 17, 44,
     },
     22},  // 0.451
    {{
         29, 21, 30, 13, 5,
     },
     19},  // 0.108
    {{
         29, 21, 30, 13, 12,
     },
     42},  //-0.059
    {{
         29, 21, 30, 13, 14,
     },
     7},  // 0.765
    {{
         29, 21, 30, 13, 18,
     },
     37},  // 0.314
    {{
         29, 21, 30, 13, 20,
     },
     5},  // 0.147
    {{
         29, 21, 30, 13, 22,
     },
     14},  // 0.235
    {{
         29, 21, 30, 13, 26,
     },
     44},  // 0.373
    {{
         29, 21, 30, 13, 31,
     },
     19},  // 0.108
    {{
         29, 21, 30, 13, 34,
     },
     43},  //-0.118
    {{
         29, 21, 30, 13, 37,
     },
     38},  //-0.020
    {{
         29, 21, 30, 13, 39,
     },
     19},  // 0.127
    {{
         29, 21, 30, 13, 42,
     },
     44},  // 0.010
    {{
         29, 21, 30, 13, 43,
     },
     37},  //-0.108
    {{
         29, 21, 30, 13, 44,
     },
     5},  // 0.196
    {{
         29, 21, 30, 13, 45,
     },
     20},  // 0.314
    {{
         29, 21, 34, 44, 13,
     },
     18},  //-0.010
    {{
         29, 21, 34, 44, 14,
     },
     22},  //-0.118
    {{
         29, 21, 34, 44, 19,
     },
     26},  // 0.157
    {{
         29, 21, 34, 44, 20,
     },
     12},  // 0.127
    {{
         29, 21, 34, 44, 26,
     },
     30},  // 0.235
    {{
         29, 21, 34, 44, 30,
     },
     13},  // 0.020
    {{
         29, 21, 34, 44, 33,
     },
     22},  // 0.147
    {{
         29, 21, 34, 44, 37,
     },
     19},  // 0.157
    {{
         29, 21, 34, 44, 41,
     },
     22},  // 0.245
    {{
         29, 21, 34, 44, 43,
     },
     26},  //-0.137
    {{
         29, 21, 34, 44, 45,
     },
     42},  // 0.127
    {{
         29, 21, 34, 44, 53,
     },
     33},  // 0.069
    {{
         29, 21, 43, 42, 13,
     },
     50},  // 0.088
    {{
         29, 21, 43, 42, 14,
     },
     7},  // 0.745
    {{
         29, 21, 43, 42, 19,
     },
     22},  // 0.373
    {{
         29, 21, 43, 42, 22,
     },
     37},  // 0.137
    {{
         29, 21, 43, 42, 26,
     },
     25},  // 0.196
    {{
         29, 21, 43, 42, 30,
     },
     49},  // 0.127
    {{
         29, 21, 43, 42, 41,
     },
     50},  //-0.265
    {{
         29, 21, 43, 42, 44,
     },
     14},  // 0.255
    {{
         29, 21, 43, 42, 45,
     },
     19},  // 0.059
    {{
         29, 21, 43, 42, 49,
     },
     56},  // 0.745
    {{
         29, 21, 43, 42, 50,
     },
     51},  //-0.147
    {{
         29, 21, 43, 42, 51,
     },
     44},  // 0.275
    {{
         29, 21, 45, 30, 18,
     },
     42},  // 0.078
    {{
         29, 21, 45, 30, 31,
     },
     18},  // 0.157
    {{
         29, 21, 45, 30, 43,
     },
     22},  // 0.010
    {{
         29, 21, 45, 30, 54,
     },
     63},  // 0.951
    {{
         34, 26, 17, 25, 8,
     },
     42},  // 0.078
    {{
         34, 26, 17, 25, 16,
     },
     24},  //-0.020
    {{
         34, 26, 17, 25, 18,
     },
     45},  // 0.127
    {{
         34, 26, 17, 25, 19,
     },
     9},  // 0.059
    {{
         34, 26, 17, 25, 20,
     },
     29},  // 0.127
    {{
         34, 26, 17, 25, 21,
     },
     44},  // 0.069
    {{
         34, 26, 17, 25, 24,
     },
     44},  //-0.206
    {{
         34, 26, 17, 25, 29,
     },
     30},  // 0.108
    {{
         34, 26, 17, 25, 33,
     },
     21},  //-0.010
    {{
         34, 26, 17, 25, 37,
     },
     9},  // 0.520
    {{
         34, 26, 17, 25, 41,
     },
     16},  // 0.147
    {{
         34, 26, 17, 25, 43,
     },
     44},  // 0.196
    {{
         34, 26, 17, 25, 44,
     },
     53},  // 0.235
    {{
         34, 26, 17, 25, 45,
     },
     9},  // 0.157
    {{
         34, 26, 18, 42, 9,
     },
     0},  // 0.873
    {{
         34, 26, 18, 42, 10,
     },
     17},  // 0.118
    {{
         34, 26, 18, 42, 20,
     },
     21},  // 0.382
    {{
         34, 26, 18, 42, 25,
     },
     37},  // 0.157
    {{
         34, 26, 18, 42, 29,
     },
     30},  //-0.020
    {{
         34, 26, 18, 42, 33,
     },
     37},  // 0.147
    {{
         34, 26, 18, 42, 37,
     },
     9},  // 0.422
    {{
         34, 26, 18, 42, 41,
     },
     40},  // 0.196
    {{
         34, 26, 18, 42, 43,
     },
     49},  // 0.304
    {{
         34, 26, 18, 42, 44,
     },
     52},  // 0.392
    {{
         34, 26, 18, 42, 45,
     },
     9},  // 0.078
    {{
         34, 26, 18, 42, 50,
     },
     37},  // 0.255
    {{
         34, 26, 19, 25, 11,
     },
     42},  // 0.186
    {{
         34, 26, 19, 25, 12,
     },
     29},  // 0.196
    {{
         34, 26, 19, 25, 16,
     },
     11},  // 0.078
    {{
         34, 26, 19, 25, 17,
     },
     44},  // 0.049
    {{
         34, 26, 19, 25, 18,
     },
     12},  // 0.049
    {{
         34, 26, 19, 25, 20,
     },
     43},  //-0.137
    {{
         34, 26, 19, 25, 21,
     },
     43},  //-0.029
    {{
         34, 26, 19, 25, 24,
     },
     10},  // 0.167
    {{
         34, 26, 19, 25, 29,
     },
     30},  //-0.225
    {{
         34, 26, 19, 25, 33,
     },
     42},  // 0.451
    {{
         34, 26, 19, 25, 37,
     },
     38},  // 0.147
    {{
         34, 26, 19, 25, 42,
     },
     49},  // 0.382
    {{
         34, 26, 19, 25, 43,
     },
     21},  // 0.049
    {{
         34, 26, 19, 25, 44,
     },
     52},  // 0.245
    {{
         34, 26, 20, 42, 12,
     },
     21},  // 0.010
    {{
         34, 26, 20, 42, 13,
     },
     18},  // 0.020
    {{
         34, 26, 20, 42, 17,
     },
     50},  //-0.088
    {{
         34, 26, 20, 42, 18,
     },
     13},  // 0.402
    {{
         34, 26, 20, 42, 19,
     },
     25},  // 0.225
    {{
         34, 26, 20, 42, 21,
     },
     12},  // 0.020
    {{
         34, 26, 20, 42, 25,
     },
     50},  // 0.049
    {{
         34, 26, 20, 42, 29,
     },
     37},  // 0.304
    {{
         34, 26, 20, 42, 33,
     },
     18},  //-0.049
    {{
         34, 26, 20, 42, 37,
     },
     13},  //-0.088
    {{
         34, 26, 20, 42, 41,
     },
     44},  // 0.088
    {{
         34, 26, 20, 42, 43,
     },
     50},  // 0.471
    {{
         34, 26, 20, 42, 44,
     },
     12},  // 0.118
    {{
         34, 26, 20, 42, 45,
     },
     37},  //-0.049
    {{
         34, 26, 20, 42, 49,
     },
     56},  // 0.706
    {{
         34, 26, 21, 41, 14,
     },
     7},  // 0.510
    {{
         34, 26, 21, 41, 17,
     },
     18},  //-0.049
    {{
         34, 26, 21, 41, 18,
     },
     10},  // 0.294
    {{
         34, 26, 21, 41, 19,
     },
     48},  // 0.078
    {{
         34, 26, 21, 41, 20,
     },
     14},  // 0.147
    {{
         34, 26, 21, 41, 25,
     },
     18},  // 0.118
    {{
         34, 26, 21, 41, 33,
     },
     19},  // 0.324
    {{
         34, 26, 21, 41, 37,
     },
     18},  // 0.010
    {{
         34, 26, 21, 41, 42,
     },
     43},  // 0.000
    {{
         34, 26, 21, 41, 44,
     },
     18},  // 0.343
    {{
         34, 26, 33, 25, 17,
     },
     18},  // 0.216
    {{
         34, 26, 33, 25, 18,
     },
     42},  // 0.147
    {{
         34, 26, 33, 25, 19,
     },
     11},  // 0.196
    {{
         34, 26, 33, 25, 20,
     },
     29},  // 0.078
    {{
         34, 26, 33, 25, 24,
     },
     19},  // 0.216
    {{
         34, 26, 33, 25, 29,
     },
     17},  // 0.167
    {{
         34, 26, 33, 25, 32,
     },
     44},  // 0.069
    {{
         34, 26, 33, 25, 37,
     },
     20},  // 0.186
    {{
         34, 26, 33, 25, 40,
     },
     44},  // 0.265
    {{
         34, 26, 33, 25, 41,
     },
     24},  // 0.088
    {{
         34, 26, 33, 25, 42,
     },
     29},  // 0.059
    {{
         34, 26, 33, 25, 43,
     },
     20},  // 0.167
    {{
         34, 26, 33, 25, 44,
     },
     41},  // 0.265
    {{
         34, 26, 33, 25, 45,
     },
     29},  // 0.206
    {{
         34, 26, 37, 42, 17,
     },
     43},  //-0.020
    {{
         34, 26, 37, 42, 19,
     },
     21},  // 0.010
    {{
         34, 26, 37, 42, 21,
     },
     50},  // 0.167
    {{
         34, 26, 37, 42, 33,
     },
     46},  // 0.225
    {{
         34, 26, 37, 42, 38,
     },
     44},  // 0.235
    {{
         34, 26, 37, 42, 49,
     },
     56},  // 1.069
    {{
         43, 26, 17, 29, 8,
     },
     25},  // 0.265
    {{
         43, 26, 17, 29, 18,
     },
     25},  //-0.216
    {{
         43, 26, 17, 29, 19,
     },
     11},  // 0.098
    {{
         43, 26, 17, 29, 20,
     },
     30},  // 0.029
    {{
         43, 26, 17, 29, 21,
     },
     30},  // 0.176
    {{
         43, 26, 17, 29, 22,
     },
     50},  //-0.020
    {{
         43, 26, 17, 29, 25,
     },
     44},  //-0.069
    {{
         43, 26, 17, 29, 30,
     },
     44},  //-0.186
    {{
         43, 26, 17, 29, 34,
     },
     21},  // 0.167
    {{
         43, 26, 17, 29, 37,
     },
     51},  // 0.137
    {{
         43, 26, 17, 29, 42,
     },
     50},  // 0.020
    {{
         43, 26, 17, 29, 44,
     },
     19},  // 0.029
    {{
         43, 26, 17, 29, 50,
     },
     25},  // 0.147
    {{
         43, 26, 17, 29, 51,
     },
     30},  // 0.373
    {{
         43, 26, 18, 25, 9,
     },
     0},  // 0.725
    {{
         43, 26, 18, 25, 10,
     },
     44},  // 0.294
    {{
         43, 26, 18, 25, 17,
     },
     44},  //-0.078
    {{
         43, 26, 18, 25, 19,
     },
     24},  // 0.275
    {{
         43, 26, 18, 25, 20,
     },
     51},  //-0.265
    {{
         43, 26, 18, 25, 21,
     },
     11},  // 0.176
    {{
         43, 26, 18, 25, 24,
     },
     42},  // 0.235
    {{
         43, 26, 18, 25, 29,
     },
     50},  // 0.078
    {{
         43, 26, 18, 25, 32,
     },
     44},  // 0.078
    {{
         43, 26, 18, 25, 34,
     },
     19},  // 0.275
    {{
         43, 26, 18, 25, 37,
     },
     46},  // 0.373
    {{
         43, 26, 18, 25, 44,
     },
     42},  // 0.176
    {{
         43, 26, 18, 25, 45,
     },
     11},  //-0.196
    {{
         43, 26, 18, 25, 50,
     },
     45},  // 0.020
    {{
         43, 26, 18, 25, 51,
     },
     44},  // 0.304
    {{
         43, 26, 19, 25, 11,
     },
     33},  // 0.029
    {{
         43, 26, 19, 25, 12,
     },
     51},  //-0.049
    {{
         43, 26, 19, 25, 18,
     },
     11},  // 0.186
    {{
         43, 26, 19, 25, 20,
     },
     21},  //-0.108
    {{
         43, 26, 19, 25, 24,
     },
     51},  // 0.304
    {{
         43, 26, 19, 25, 29,
     },
     21},  //-0.059
    {{
         43, 26, 19, 25, 33,
     },
     17},  // 0.167
    {{
         43, 26, 19, 25, 34,
     },
     44},  // 0.167
    {{
         43, 26, 19, 25, 37,
     },
     51},  //-0.206
    {{
         43, 26, 19, 25, 44,
     },
     52},  //-0.147
    {{
         43, 26, 19, 25, 50,
     },
     11},  // 0.206
    {{
         43, 26, 19, 25, 51,
     },
     11},  // 0.255
    {{
         43, 26, 20, 29, 12,
     },
     44},  //-0.088
    {{
         43, 26, 20, 29, 13,
     },
     38},  // 0.304
    {{
         43, 26, 20, 29, 17,
     },
     42},  // 0.176
    {{
         43, 26, 20, 29, 18,
     },
     44},  // 0.098
    {{
         43, 26, 20, 29, 19,
     },
     10},  // 0.098
    {{
         43, 26, 20, 29, 21,
     },
     25},  // 0.294
    {{
         43, 26, 20, 29, 22,
     },
     34},  // 0.422
    {{
         43, 26, 20, 29, 25,
     },
     24},  // 0.029
    {{
         43, 26, 20, 29, 30,
     },
     44},  //-0.333
    {{
         43, 26, 20, 29, 34,
     },
     12},  // 0.137
    {{
         43, 26, 20, 29, 37,
     },
     30},  // 0.167
    {{
         43, 26, 20, 29, 38,
     },
     25},  // 0.186
    {{
         43, 26, 20, 29, 44,
     },
     12},  // 0.382
    {{
         43, 26, 20, 29, 45,
     },
     34},  // 0.118
    {{
         43, 26, 20, 29, 50,
     },
     12},  // 0.167
    {{
         43, 26, 20, 29, 51,
     },
     19},  // 0.098
    {{
         43, 26, 21, 45, 14,
     },
     7},  // 1.010
    {{
         43, 26, 21, 45, 17,
     },
     51},  // 0.373
    {{
         43, 26, 21, 45, 19,
     },
     14},  // 0.275
    {{
         43, 26, 21, 45, 25,
     },
     34},  // 0.098
    {{
         43, 26, 21, 45, 29,
     },
     30},  // 0.108
    {{
         43, 26, 21, 45, 37,
     },
     18},  // 0.186
    {{
         43, 26, 21, 45, 42,
     },
     14},  // 0.020
    {{
         43, 26, 21, 45, 44,
     },
     18},  // 0.225
    {{
         43, 26, 21, 45, 51,
     },
     50},  //-0.039
    {{
         43, 26, 29, 45, 17,
     },
     18},  //-0.157
    {{
         43, 26, 29, 45, 18,
     },
     44},  //-0.118
    {{
         43, 26, 29, 45, 19,
     },
     21},  // 0.216
    {{
         43, 26, 29, 45, 20,
     },
     30},  //-0.157
    {{
         43, 26, 29, 45, 21,
     },
     54},  // 0.284
    {{
         43, 26, 29, 45, 22,
     },
     20},  // 0.108
    {{
         43, 26, 29, 45, 25,
     },
     18},  //-0.206
    {{
         43, 26, 29, 45, 34,
     },
     42},  // 0.186
    {{
         43, 26, 29, 45, 37,
     },
     30},  // 0.157
    {{
         43, 26, 29, 45, 50,
     },
     30},  // 0.108
    {{
         43, 26, 29, 45, 51,
     },
     30},  //-0.108
    {{
         43, 26, 29, 45, 54,
     },
     63},  // 0.912
    {{
         43, 26, 34, 33, 18,
     },
     10},  //-0.098
    {{
         43, 26, 34, 33, 19,
     },
     18},  // 0.137
    {{
         43, 26, 34, 33, 20,
     },
     44},  // 0.265
    {{
         43, 26, 34, 33, 25,
     },
     37},  // 0.147
    {{
         43, 26, 34, 33, 29,
     },
     42},  // 0.147
    {{
         43, 26, 34, 33, 32,
     },
     41},  // 0.029
    {{
         43, 26, 34, 33, 50,
     },
     17},  // 0.157
    {{
         43, 26, 37, 34, 19,
     },
     20},  // 0.118
    {{
         43, 26, 37, 34, 25,
     },
     19},  // 0.147
    {{
         43, 26, 37, 34, 29,
     },
     42},  // 0.206
    {{
         43, 26, 37, 34, 33,
     },
     44},  // 0.304
    {{
         43, 26, 50, 25, 17,
     },
     57},  // 0.118
    {{
         43, 26, 50, 25, 18,
     },
     42},  // 0.029
    {{
         43, 26, 50, 25, 19,
     },
     11},  // 0.255
    {{
         43, 26, 50, 25, 20,
     },
     57},  // 0.020
    {{
         43, 26, 50, 25, 21,
     },
     19},  // 0.225
    {{
         43, 26, 50, 25, 24,
     },
     44},  // 0.127
    {{
         43, 26, 50, 25, 29,
     },
     18},  // 0.108
    {{
         43, 26, 50, 25, 34,
     },
     42},  //-0.069
    {{
         43, 26, 50, 25, 37,
     },
     57},  // 0.275
    {{
         43, 26, 50, 25, 44,
     },
     42},  // 0.137
    {{
         43, 26, 50, 25, 45,
     },
     42},  // 0.186
    {{
         43, 26, 50, 25, 51,
     },
     17},  // 0.206
    {{
         43, 26, 50, 25, 57,
     },
     29},  //-0.127
    {{
         43, 26, 51, 25, 17,
     },
     19},  // 0.147
    {{
         43, 26, 51, 25, 18,
     },
     29},  // 0.167
    {{
         43, 26, 51, 25, 19,
     },
     42},  // 0.049
    {{
         43, 26, 51, 25, 20,
     },
     29},  // 0.049
    {{
         43, 26, 51, 25, 21,
     },
     19},  // 0.157
    {{
         43, 26, 51, 25, 24,
     },
     42},  // 0.186
    {{
         43, 26, 51, 25, 29,
     },
     30},  // 0.029
    {{
         43, 26, 51, 25, 34,
     },
     37},  // 0.059
    {{
         43, 26, 51, 25, 37,
     },
     19},  // 0.225
    {{
         43, 26, 51, 25, 44,
     },
     29},  // 0.020
    {{
         43, 26, 51, 25, 45,
     },
     44},  //-0.059
    {{
         43, 26, 51, 25, 50,
     },
     44},  //-0.157
    {{
         43, 26, 51, 25, 59,
     },
     29},  // 0.294
    {{
         45, 37, 18, 21, 9,
     },
     0},  // 0.578
    {{
         45, 37, 18, 21, 14,
     },
     7},  // 1.284
    {{
         45, 37, 18, 21, 26,
     },
     38},  //-0.010
    {{
         45, 37, 18, 21, 29,
     },
     43},  // 0.000
    {{
         45, 37, 18, 21, 42,
     },
     53},  // 0.314
    {{
         45, 37, 18, 21, 44,
     },
     38},  // 0.000
    {{
         45, 37, 18, 21, 46,
     },
     38},  // 0.255
    {{
         45, 37, 18, 21, 54,
     },
     63},  // 0.706
    {{
         45, 37, 20, 38, 12,
     },
     44},  // 0.069
    {{
         45, 37, 20, 38, 13,
     },
     21},  // 0.147
    {{
         45, 37, 20, 38, 18,
     },
     39},  // 0.196
    {{
         45, 37, 20, 38, 19,
     },
     12},  // 0.382
    {{
         45, 37, 20, 38, 26,
     },
     25},  // 0.147
    {{
         45, 37, 20, 38, 29,
     },
     11},  // 0.069
    {{
         45, 37, 20, 38, 31,
     },
     12},  // 0.000
    {{
         45, 37, 20, 38, 34,
     },
     29},  // 0.118
    {{
         45, 37, 20, 38, 39,
     },
     19},  // 0.255
    {{
         45, 37, 20, 38, 42,
     },
     34},  // 0.412
    {{
         45, 37, 20, 38, 43,
     },
     34},  //-0.147
    {{
         45, 37, 20, 38, 44,
     },
     26},  // 0.167
    {{
         45, 37, 20, 38, 46,
     },
     53},  // 0.059
    {{
         45, 37, 20, 38, 53,
     },
     19},  // 0.206
    {{
         45, 37, 20, 38, 54,
     },
     63},  // 0.598
    {{
         45, 37, 29, 21, 13,
     },
     46},  // 0.167
    {{
         45, 37, 29, 21, 18,
     },
     34},  // 0.255
    {{
         45, 37, 29, 21, 19,
     },
     38},  // 0.490
    {{
         45, 37, 29, 21, 20,
     },
     38},  //-0.098
    {{
         45, 37, 29, 21, 22,
     },
     53},  // 0.176
    {{
         45, 37, 29, 21, 26,
     },
     54},  // 0.343
    {{
         45, 37, 29, 21, 30,
     },
     38},  // 0.049
    {{
         45, 37, 29, 21, 34,
     },
     26},  // 0.235
    {{
         45, 37, 29, 21, 38,
     },
     53},  // 0.098
    {{
         45, 37, 29, 21, 43,
     },
     44},  //-0.069
    {{
         45, 37, 29, 21, 53,
     },
     46},  // 0.118
    {{
         45, 37, 29, 21, 54,
     },
     63},  // 0.637
    {{
         45, 37, 34, 44, 18,
     },
     30},  // 0.275
    {{
         45, 37, 34, 44, 19,
     },
     42},  // 0.324
    {{
         45, 37, 34, 44, 20,
     },
     43},  // 0.324
    {{
         45, 37, 34, 44, 21,
     },
     52},  // 0.284
    {{
         45, 37, 34, 44, 29,
     },
     20},  // 0.176
    {{
         45, 37, 34, 44, 33,
     },
     51},  // 0.235
    {{
         45, 37, 34, 44, 38,
     },
     19},  // 0.137
    {{
         45, 37, 34, 44, 41,
     },
     38},  // 0.265
    {{
         45, 37, 34, 44, 43,
     },
     19},  // 0.333
    {{
         45, 37, 34, 44, 53,
     },
     19},  // 0.216
    {{
         45, 37, 34, 44, 54,
     },
     63},  // 0.990
    {{
         45, 37, 38, 42, 18,
     },
     46},  // 0.020
    {{
         45, 37, 38, 42, 19,
     },
     54},  // 0.265
    {{
         45, 37, 38, 42, 20,
     },
     21},  //-0.422
    {{
         45, 37, 38, 42, 21,
     },
     14},  //-0.049
    {{
         45, 37, 38, 42, 29,
     },
     39},  //-0.206
    {{
         45, 37, 38, 42, 31,
     },
     34},  // 0.039
    {{
         45, 37, 38, 42, 34,
     },
     26},  // 0.324
    {{
         45, 37, 38, 42, 39,
     },
     20},  // 0.255
    {{
         45, 37, 38, 42, 43,
     },
     44},  // 0.147
    {{
         45, 37, 38, 42, 49,
     },
     56},  // 0.824
    {{
         45, 37, 38, 42, 52,
     },
     46},  //-0.108
    {{
         45, 37, 38, 42, 53,
     },
     31},  // 0.039
    {{
         45, 37, 38, 42, 54,
     },
     63},  // 0.667
    {{
         45, 37, 43, 53, 18,
     },
     42},  // 0.284
    {{
         45, 37, 43, 53, 19,
     },
     18},  //-0.029
    {{
         45, 37, 43, 53, 20,
     },
     29},  // 0.029
    {{
         45, 37, 43, 53, 21,
     },
     29},  //-0.284
    {{
         45, 37, 43, 53, 29,
     },
     18},  // 0.000
    {{
         45, 37, 43, 53, 34,
     },
     44},  // 0.186
    {{
         45, 37, 43, 53, 38,
     },
     20},  // 0.088
    {{
         45, 37, 43, 53, 50,
     },
     19},  // 0.324
    {{
         45, 37, 43, 53, 51,
     },
     34},  // 0.245
    {{
         45, 37, 43, 53, 54,
     },
     63},  // 0.627
    {{
         45, 37, 54, 63, 18,
     },
     46},  // 1.343
    {{
         45, 37, 54, 63, 29,
     },
     20},  // 1.088
    {{
         45, 37, 54, 63, 43,
     },
     19},  // 1.225
};
}

void print_first_move() { find_best_move({}); }

void print_second_move() {
  Board board;
  for (int move : board) {
    find_best_move({move});
  }
}

void print_opening(int t) {
  for (const auto& entry : OPENING_DATA) {
    if (entry.first.size() + 3 == t) {
      Board board;
      for (int move : entry.first) {
        board.do_move(move);
      }
      board.do_move(entry.second);

      for (int move : board) {
        auto moves = entry.first;
        moves.push_back(entry.second);
        moves.push_back(move);
        find_best_move(moves);
      }
    }
  }
}

int main() {
  printf("//5th\n");
  print_opening(5);
  printf("//6th\n");
  print_opening(6);
  return 0;
}
