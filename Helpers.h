#pragma once

#include <chrono>
#include <random>

class Board;

namespace rng {
int pick(int bound);
}

struct UniformRandomGenerator {
  UniformRandomGenerator();

  int operator()(int bound);

  std::random_device rd;
  std::mt19937 g;
};

namespace timer {
using TimePoint = std::chrono::steady_clock::time_point;

TimePoint now();
int get_delta_time_since(const TimePoint& start);
}  // namespace timer

struct IOStream {
  bool get();
  bool start() const;
  bool quit() const;
  void print(int move);
  int move() const;

  char s[1024];
};

struct Logger {
  enum Level { TRACE = 0, INFO = 1, ERROR = 2 };

  static Level level;
  static int info(const char* fmt, ...);
};
