#include "Helpers.h"

#include "Board.h"

#include <cstdarg>
#include <cstring>
#include <random>

using namespace std;

UniformRandomGenerator::UniformRandomGenerator() : rd{}, g{rd()} {}

int UniformRandomGenerator::operator()(int bound) {
  return uniform_int_distribution<>{0, bound - 1}(g);
}

namespace timer {
TimePoint now() { return chrono::steady_clock::now(); }

int get_delta_time_since(const TimePoint& start) {
  using chrono::duration_cast;
  using chrono::milliseconds;
  const auto current = now();
  return duration_cast<milliseconds>(current - start).count();
}
}  // namespace timer

namespace {
// clang-format off
  constexpr char FIELDS[NXN][3] {
    "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8",
    "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8",
    "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8",
    "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8",
    "E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8",
    "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8",
    "G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8",
    "H1", "H2", "H3", "H4", "H5", "H6", "H7", "H8"};
// clang-format on

int prflush(const char* fmt, ...) {
  int r;
  va_list ap;
  va_start(ap, fmt);
  r = vprintf(fmt, ap);
  va_end(ap);
  fflush(stdout);
  return r;
}
}  // namespace

bool IOStream::get() { return scanf("%s", s) == 1; }

bool IOStream::start() const { return strncmp(s, "Start", 5) == 0; }

bool IOStream::quit() const { return strncmp(s, "Quit", 4) == 0; }

int IOStream::move() const {
  int x = s[1] - '1';
  int y = s[0] - 'A';
  return x + (y << 3);
}

void IOStream::print(int move) {
  if (move >= 0 and move < NXN) {
    prflush("%s\n", FIELDS[move]);
  } else {
    prflush("invalid-move(%d)\n", move);
  }
}

Logger::Level Logger::level = Logger::INFO;

int Logger::info(const char* fmt, ...) {
  if (level > Logger::INFO) {
    return 0;
  }

  va_list ap;
  va_start(ap, fmt);
  int r = vfprintf(stderr, fmt, ap);
  va_end(ap);
  fflush(stderr);
  return r;
}
