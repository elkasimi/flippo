#! /bin/bash

SUBMISSION=Submission.cpp
cat Helpers.h \
    Board.h \
    TreeNode.h \
    MCTS.h \
    Helpers.cpp \
    Board.cpp \
    TreeNode.cpp \
    MCTS.cpp \
    main.cpp > ${SUBMISSION}
sed -i '' '/#include "/d' ${SUBMISSION}
sed -i '' '/#pragma once/d' ${SUBMISSION}
