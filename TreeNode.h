#pragma once

#include "Board.h"

#include <cstddef>
#include <vector>

struct UniformRandomGenerator;
struct TreeNodeAllocator;

struct TreeNode {
  TreeNode();
  TreeNode(TreeNode *parent, const Board &board);

  TreeNode *expand(TreeNodeAllocator &allocator, Board &board);
  TreeNode *select(UniformRandomGenerator &g) const;
  TreeNode *select_most_visited() const;
  TreeNode *select_best() const;
  TreeNode *select_randomly(UniformRandomGenerator &g) const;
  TreeNode *select(int move) const;
  bool leaf() const;
  bool fully_expanded() const;
  void update(const int &payoff);
  void dispose();
  void dispose_all();

  struct Iterator {
    Iterator(TreeNode *node);

    void operator++();
    TreeNode *operator*();
    bool operator!=(const Iterator &other) const;

    TreeNode *node;
  };
  Iterator begin() const;
  Iterator end() const;

  TreeNode *parent;
  TreeNode *first_child;
  TreeNode *next;
  int player;
  int move;
  int untried_count;
  int untried_moves[NXN];
  double value;
  int visits;
  bool available;
};

struct TreeNodeAllocator {
  TreeNodeAllocator();

  TreeNode *operator()();

  std::vector<TreeNode> nodes;
  int index;
};

TreeNode *make_root(TreeNodeAllocator &allocator, const Board &board);
TreeNode *make_tree_node(TreeNodeAllocator &allocator, TreeNode *parent,
                         const Board &board);
