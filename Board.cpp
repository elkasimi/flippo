#include "Board.h"

#include "Helpers.h"

#include <algorithm>
#include <iterator>
#include <vector>

using namespace std;

namespace {
constexpr uint64_t ONE{1ull};
constexpr uint64_t ZERO{0ull};

constexpr int ITERATORS_COUNT{8};
const vector<int> MOVE_ITERATORS[NXN][ITERATORS_COUNT]{
    {
        {},
        {},
        {},
        {},
        {8, 16, 24, 32, 40, 48, 56},
        {},
        {1, 2, 3, 4, 5, 6, 7},
        {9, 18, 27, 36, 45, 54, 63},
    },
    {
        {},
        {0},
        {8},
        {},
        {9, 17, 25, 33, 41, 49, 57},
        {},
        {2, 3, 4, 5, 6, 7},
        {10, 19, 28, 37, 46, 55},
    },
    {
        {},
        {1, 0},
        {9, 16},
        {},
        {10, 18, 26, 34, 42, 50, 58},
        {},
        {3, 4, 5, 6, 7},
        {11, 20, 29, 38, 47},
    },
    {
        {},
        {2, 1, 0},
        {10, 17, 24},
        {},
        {11, 19, 27, 35, 43, 51, 59},
        {},
        {4, 5, 6, 7},
        {12, 21, 30, 39},
    },
    {
        {},
        {3, 2, 1, 0},
        {11, 18, 25, 32},
        {},
        {12, 20, 28, 36, 44, 52, 60},
        {},
        {5, 6, 7},
        {13, 22, 31},
    },
    {
        {},
        {4, 3, 2, 1, 0},
        {12, 19, 26, 33, 40},
        {},
        {13, 21, 29, 37, 45, 53, 61},
        {},
        {6, 7},
        {14, 23},
    },
    {
        {},
        {5, 4, 3, 2, 1, 0},
        {13, 20, 27, 34, 41, 48},
        {},
        {14, 22, 30, 38, 46, 54, 62},
        {},
        {7},
        {15},
    },
    {
        {},
        {6, 5, 4, 3, 2, 1, 0},
        {14, 21, 28, 35, 42, 49, 56},
        {},
        {15, 23, 31, 39, 47, 55, 63},
        {},
        {},
        {},
    },
    {
        {},
        {},
        {},
        {0},
        {16, 24, 32, 40, 48, 56},
        {1},
        {9, 10, 11, 12, 13, 14, 15},
        {17, 26, 35, 44, 53, 62},
    },
    {
        {0},
        {8},
        {16},
        {1},
        {17, 25, 33, 41, 49, 57},
        {2},
        {10, 11, 12, 13, 14, 15},
        {18, 27, 36, 45, 54, 63},
    },
    {
        {1},
        {9, 8},
        {17, 24},
        {2},
        {18, 26, 34, 42, 50, 58},
        {3},
        {11, 12, 13, 14, 15},
        {19, 28, 37, 46, 55},
    },
    {
        {2},
        {10, 9, 8},
        {18, 25, 32},
        {3},
        {19, 27, 35, 43, 51, 59},
        {4},
        {12, 13, 14, 15},
        {20, 29, 38, 47},
    },
    {
        {3},
        {11, 10, 9, 8},
        {19, 26, 33, 40},
        {4},
        {20, 28, 36, 44, 52, 60},
        {5},
        {13, 14, 15},
        {21, 30, 39},
    },
    {
        {4},
        {12, 11, 10, 9, 8},
        {20, 27, 34, 41, 48},
        {5},
        {21, 29, 37, 45, 53, 61},
        {6},
        {14, 15},
        {22, 31},
    },
    {
        {5},
        {13, 12, 11, 10, 9, 8},
        {21, 28, 35, 42, 49, 56},
        {6},
        {22, 30, 38, 46, 54, 62},
        {7},
        {15},
        {23},
    },
    {
        {6},
        {14, 13, 12, 11, 10, 9, 8},
        {22, 29, 36, 43, 50, 57},
        {7},
        {23, 31, 39, 47, 55, 63},
        {},
        {},
        {},
    },
    {
        {},
        {},
        {},
        {8, 0},
        {24, 32, 40, 48, 56},
        {9, 2},
        {17, 18, 19, 20, 21, 22, 23},
        {25, 34, 43, 52, 61},
    },
    {
        {8},
        {16},
        {24},
        {9, 1},
        {25, 33, 41, 49, 57},
        {10, 3},
        {18, 19, 20, 21, 22, 23},
        {26, 35, 44, 53, 62},
    },
    {
        {9, 0},
        {17, 16},
        {25, 32},
        {10, 2},
        {26, 34, 42, 50, 58},
        {11, 4},
        {19, 20, 21, 22, 23},
        {27, 36, 45, 54, 63},
    },
    {
        {10, 1},
        {18, 17, 16},
        {26, 33, 40},
        {11, 3},
        {27, 35, 43, 51, 59},
        {12, 5},
        {20, 21, 22, 23},
        {28, 37, 46, 55},
    },
    {
        {11, 2},
        {19, 18, 17, 16},
        {27, 34, 41, 48},
        {12, 4},
        {28, 36, 44, 52, 60},
        {13, 6},
        {21, 22, 23},
        {29, 38, 47},
    },
    {
        {12, 3},
        {20, 19, 18, 17, 16},
        {28, 35, 42, 49, 56},
        {13, 5},
        {29, 37, 45, 53, 61},
        {14, 7},
        {22, 23},
        {30, 39},
    },
    {
        {13, 4},
        {21, 20, 19, 18, 17, 16},
        {29, 36, 43, 50, 57},
        {14, 6},
        {30, 38, 46, 54, 62},
        {15},
        {23},
        {31},
    },
    {
        {14, 5},
        {22, 21, 20, 19, 18, 17, 16},
        {30, 37, 44, 51, 58},
        {15, 7},
        {31, 39, 47, 55, 63},
        {},
        {},
        {},
    },
    {
        {},
        {},
        {},
        {16, 8, 0},
        {32, 40, 48, 56},
        {17, 10, 3},
        {25, 26, 27, 28, 29, 30, 31},
        {33, 42, 51, 60},
    },
    {
        {16},
        {24},
        {32},
        {17, 9, 1},
        {33, 41, 49, 57},
        {18, 11, 4},
        {26, 27, 28, 29, 30, 31},
        {34, 43, 52, 61},
    },
    {
        {17, 8},
        {25, 24},
        {33, 40},
        {18, 10, 2},
        {34, 42, 50, 58},
        {19, 12, 5},
        {27, 28, 29, 30, 31},
        {35, 44, 53, 62},
    },
    {
        {18, 9, 0},
        {26, 25, 24},
        {34, 41, 48},
        {19, 11, 3},
        {35, 43, 51, 59},
        {20, 13, 6},
        {28, 29, 30, 31},
        {36, 45, 54, 63},
    },
    {
        {19, 10, 1},
        {27, 26, 25, 24},
        {35, 42, 49, 56},
        {20, 12, 4},
        {36, 44, 52, 60},
        {21, 14, 7},
        {29, 30, 31},
        {37, 46, 55},
    },
    {
        {20, 11, 2},
        {28, 27, 26, 25, 24},
        {36, 43, 50, 57},
        {21, 13, 5},
        {37, 45, 53, 61},
        {22, 15},
        {30, 31},
        {38, 47},
    },
    {
        {21, 12, 3},
        {29, 28, 27, 26, 25, 24},
        {37, 44, 51, 58},
        {22, 14, 6},
        {38, 46, 54, 62},
        {23},
        {31},
        {39},
    },
    {
        {22, 13, 4},
        {30, 29, 28, 27, 26, 25, 24},
        {38, 45, 52, 59},
        {23, 15, 7},
        {39, 47, 55, 63},
        {},
        {},
        {},
    },
    {
        {},
        {},
        {},
        {24, 16, 8, 0},
        {40, 48, 56},
        {25, 18, 11, 4},
        {33, 34, 35, 36, 37, 38, 39},
        {41, 50, 59},
    },
    {
        {24},
        {32},
        {40},
        {25, 17, 9, 1},
        {41, 49, 57},
        {26, 19, 12, 5},
        {34, 35, 36, 37, 38, 39},
        {42, 51, 60},
    },
    {
        {25, 16},
        {33, 32},
        {41, 48},
        {26, 18, 10, 2},
        {42, 50, 58},
        {27, 20, 13, 6},
        {35, 36, 37, 38, 39},
        {43, 52, 61},
    },
    {
        {26, 17, 8},
        {34, 33, 32},
        {42, 49, 56},
        {27, 19, 11, 3},
        {43, 51, 59},
        {28, 21, 14, 7},
        {36, 37, 38, 39},
        {44, 53, 62},
    },
    {
        {27, 18, 9, 0},
        {35, 34, 33, 32},
        {43, 50, 57},
        {28, 20, 12, 4},
        {44, 52, 60},
        {29, 22, 15},
        {37, 38, 39},
        {45, 54, 63},
    },
    {
        {28, 19, 10, 1},
        {36, 35, 34, 33, 32},
        {44, 51, 58},
        {29, 21, 13, 5},
        {45, 53, 61},
        {30, 23},
        {38, 39},
        {46, 55},
    },
    {
        {29, 20, 11, 2},
        {37, 36, 35, 34, 33, 32},
        {45, 52, 59},
        {30, 22, 14, 6},
        {46, 54, 62},
        {31},
        {39},
        {47},
    },
    {
        {30, 21, 12, 3},
        {38, 37, 36, 35, 34, 33, 32},
        {46, 53, 60},
        {31, 23, 15, 7},
        {47, 55, 63},
        {},
        {},
        {},
    },
    {
        {},
        {},
        {},
        {32, 24, 16, 8, 0},
        {48, 56},
        {33, 26, 19, 12, 5},
        {41, 42, 43, 44, 45, 46, 47},
        {49, 58},
    },
    {
        {32},
        {40},
        {48},
        {33, 25, 17, 9, 1},
        {49, 57},
        {34, 27, 20, 13, 6},
        {42, 43, 44, 45, 46, 47},
        {50, 59},
    },
    {
        {33, 24},
        {41, 40},
        {49, 56},
        {34, 26, 18, 10, 2},
        {50, 58},
        {35, 28, 21, 14, 7},
        {43, 44, 45, 46, 47},
        {51, 60},
    },
    {
        {34, 25, 16},
        {42, 41, 40},
        {50, 57},
        {35, 27, 19, 11, 3},
        {51, 59},
        {36, 29, 22, 15},
        {44, 45, 46, 47},
        {52, 61},
    },
    {
        {35, 26, 17, 8},
        {43, 42, 41, 40},
        {51, 58},
        {36, 28, 20, 12, 4},
        {52, 60},
        {37, 30, 23},
        {45, 46, 47},
        {53, 62},
    },
    {
        {36, 27, 18, 9, 0},
        {44, 43, 42, 41, 40},
        {52, 59},
        {37, 29, 21, 13, 5},
        {53, 61},
        {38, 31},
        {46, 47},
        {54, 63},
    },
    {
        {37, 28, 19, 10, 1},
        {45, 44, 43, 42, 41, 40},
        {53, 60},
        {38, 30, 22, 14, 6},
        {54, 62},
        {39},
        {47},
        {55},
    },
    {
        {38, 29, 20, 11, 2},
        {46, 45, 44, 43, 42, 41, 40},
        {54, 61},
        {39, 31, 23, 15, 7},
        {55, 63},
        {},
        {},
        {},
    },
    {
        {},
        {},
        {},
        {40, 32, 24, 16, 8, 0},
        {56},
        {41, 34, 27, 20, 13, 6},
        {49, 50, 51, 52, 53, 54, 55},
        {57},
    },
    {
        {40},
        {48},
        {56},
        {41, 33, 25, 17, 9, 1},
        {57},
        {42, 35, 28, 21, 14, 7},
        {50, 51, 52, 53, 54, 55},
        {58},
    },
    {
        {41, 32},
        {49, 48},
        {57},
        {42, 34, 26, 18, 10, 2},
        {58},
        {43, 36, 29, 22, 15},
        {51, 52, 53, 54, 55},
        {59},
    },
    {
        {42, 33, 24},
        {50, 49, 48},
        {58},
        {43, 35, 27, 19, 11, 3},
        {59},
        {44, 37, 30, 23},
        {52, 53, 54, 55},
        {60},
    },
    {
        {43, 34, 25, 16},
        {51, 50, 49, 48},
        {59},
        {44, 36, 28, 20, 12, 4},
        {60},
        {45, 38, 31},
        {53, 54, 55},
        {61},
    },
    {
        {44, 35, 26, 17, 8},
        {52, 51, 50, 49, 48},
        {60},
        {45, 37, 29, 21, 13, 5},
        {61},
        {46, 39},
        {54, 55},
        {62},
    },
    {
        {45, 36, 27, 18, 9, 0},
        {53, 52, 51, 50, 49, 48},
        {61},
        {46, 38, 30, 22, 14, 6},
        {62},
        {47},
        {55},
        {63},
    },
    {
        {46, 37, 28, 19, 10, 1},
        {54, 53, 52, 51, 50, 49, 48},
        {62},
        {47, 39, 31, 23, 15, 7},
        {63},
        {},
        {},
        {},
    },
    {
        {},
        {},
        {},
        {48, 40, 32, 24, 16, 8, 0},
        {},
        {49, 42, 35, 28, 21, 14, 7},
        {57, 58, 59, 60, 61, 62, 63},
        {},
    },
    {
        {48},
        {56},
        {},
        {49, 41, 33, 25, 17, 9, 1},
        {},
        {50, 43, 36, 29, 22, 15},
        {58, 59, 60, 61, 62, 63},
        {},
    },
    {
        {49, 40},
        {57, 56},
        {},
        {50, 42, 34, 26, 18, 10, 2},
        {},
        {51, 44, 37, 30, 23},
        {59, 60, 61, 62, 63},
        {},
    },
    {
        {50, 41, 32},
        {58, 57, 56},
        {},
        {51, 43, 35, 27, 19, 11, 3},
        {},
        {52, 45, 38, 31},
        {60, 61, 62, 63},
        {},
    },
    {
        {51, 42, 33, 24},
        {59, 58, 57, 56},
        {},
        {52, 44, 36, 28, 20, 12, 4},
        {},
        {53, 46, 39},
        {61, 62, 63},
        {},
    },
    {
        {52, 43, 34, 25, 16},
        {60, 59, 58, 57, 56},
        {},
        {53, 45, 37, 29, 21, 13, 5},
        {},
        {54, 47},
        {62, 63},
        {},
    },
    {
        {53, 44, 35, 26, 17, 8},
        {61, 60, 59, 58, 57, 56},
        {},
        {54, 46, 38, 30, 22, 14, 6},
        {},
        {55},
        {63},
        {},
    },
    {
        {54, 45, 36, 27, 18, 9, 0},
        {62, 61, 60, 59, 58, 57, 56},
        {},
        {55, 47, 39, 31, 23, 15, 7},
        {},
        {},
        {},
        {},
    }};

const vector<int> NEIGHBOURS[NXN]{{8, 1, 9},
                                  {0, 8, 9, 2, 10},
                                  {1, 9, 10, 3, 11},
                                  {2, 10, 11, 4, 12},
                                  {3, 11, 12, 5, 13},
                                  {4, 12, 13, 6, 14},
                                  {5, 13, 14, 7, 15},
                                  {6, 14, 15},
                                  {0, 16, 1, 9, 17},
                                  {0, 8, 16, 1, 17, 2, 10, 18},
                                  {1, 9, 17, 2, 18, 3, 11, 19},
                                  {2, 10, 18, 3, 19, 4, 12, 20},
                                  {3, 11, 19, 4, 20, 5, 13, 21},
                                  {4, 12, 20, 5, 21, 6, 14, 22},
                                  {5, 13, 21, 6, 22, 7, 15, 23},
                                  {6, 14, 22, 7, 23},
                                  {8, 24, 9, 17, 25},
                                  {8, 16, 24, 9, 25, 10, 18, 26},
                                  {9, 17, 25, 10, 26, 11, 19, 27},
                                  {10, 18, 26, 11, 27, 12, 20, 28},
                                  {11, 19, 27, 12, 28, 13, 21, 29},
                                  {12, 20, 28, 13, 29, 14, 22, 30},
                                  {13, 21, 29, 14, 30, 15, 23, 31},
                                  {14, 22, 30, 15, 31},
                                  {16, 32, 17, 25, 33},
                                  {16, 24, 32, 17, 33, 18, 26, 34},
                                  {17, 25, 33, 18, 34, 19, 27, 35},
                                  {18, 26, 34, 19, 35, 20, 28, 36},
                                  {19, 27, 35, 20, 36, 21, 29, 37},
                                  {20, 28, 36, 21, 37, 22, 30, 38},
                                  {21, 29, 37, 22, 38, 23, 31, 39},
                                  {22, 30, 38, 23, 39},
                                  {24, 40, 25, 33, 41},
                                  {24, 32, 40, 25, 41, 26, 34, 42},
                                  {25, 33, 41, 26, 42, 27, 35, 43},
                                  {26, 34, 42, 27, 43, 28, 36, 44},
                                  {27, 35, 43, 28, 44, 29, 37, 45},
                                  {28, 36, 44, 29, 45, 30, 38, 46},
                                  {29, 37, 45, 30, 46, 31, 39, 47},
                                  {30, 38, 46, 31, 47},
                                  {32, 48, 33, 41, 49},
                                  {32, 40, 48, 33, 49, 34, 42, 50},
                                  {33, 41, 49, 34, 50, 35, 43, 51},
                                  {34, 42, 50, 35, 51, 36, 44, 52},
                                  {35, 43, 51, 36, 52, 37, 45, 53},
                                  {36, 44, 52, 37, 53, 38, 46, 54},
                                  {37, 45, 53, 38, 54, 39, 47, 55},
                                  {38, 46, 54, 39, 55},
                                  {40, 56, 41, 49, 57},
                                  {40, 48, 56, 41, 57, 42, 50, 58},
                                  {41, 49, 57, 42, 58, 43, 51, 59},
                                  {42, 50, 58, 43, 59, 44, 52, 60},
                                  {43, 51, 59, 44, 60, 45, 53, 61},
                                  {44, 52, 60, 45, 61, 46, 54, 62},
                                  {45, 53, 61, 46, 62, 47, 55, 63},
                                  {46, 54, 62, 47, 63},
                                  {48, 49, 57},
                                  {48, 56, 49, 50, 58},
                                  {49, 57, 50, 51, 59},
                                  {50, 58, 51, 52, 60},
                                  {51, 59, 52, 53, 61},
                                  {52, 60, 53, 54, 62},
                                  {53, 61, 54, 55, 63},
                                  {54, 62, 55}};

constexpr uint64_t START_WHITE_PIECES = ONE << 27 | ONE << 36;
constexpr uint64_t START_BLACK_PIECES = ONE << 28 | ONE << 35;
constexpr uint64_t START_NEIGHBOUR_MOVES =
    ONE << 18 | ONE << 19 | ONE << 20 | ONE << 21 | ONE << 26 | ONE << 29 |
    ONE << 34 | ONE << 37 | ONE << 42 | ONE << 43 | ONE << 44 | ONE << 45;
constexpr uint64_t START_FILLED = ONE << 27 | ONE << 28 | ONE << 35 | ONE << 36;
}  // namespace

Board::MoveIterator::MoveIterator(uint64_t c) : code(c) {}

void Board::MoveIterator::operator++() { code &= code - 1; }

int Board::MoveIterator::operator*() { return __builtin_ffsll(code) - 1; }

bool Board::MoveIterator::operator!=(const MoveIterator& other) const {
  return code != other.code;
}

Board::Board()
    : m_pieces{START_WHITE_PIECES, START_BLACK_PIECES},
      m_neighbour_moves{START_NEIGHBOUR_MOVES},
      m_filled{START_FILLED},
      m_turns{0},
      m_last_move(-1) {}

int Board::last_moved() const { return m_turns & 1 ? WHITE : BLACK; }

int Board::last_move() const { return m_last_move; }

Board::MoveIterator Board::begin() const {
  uint64_t code{ZERO};
  for (auto moves = m_neighbour_moves; moves; moves &= moves - 1) {
    int move = __builtin_ffsll(moves) - 1;
    bool flip = any_of(
        std::begin(MOVE_ITERATORS[move]), std::end(MOVE_ITERATORS[move]),
        [this](const vector<int>& fields) {
          if (fields.empty() or not filled(fields.front())) {
            return false;
          }

          auto it = find_if(fields.begin() + 1, fields.end(),
                            [this](int i) { return not filled(i) or owns(i); });

          return it != fields.end() and owns(*it);
        });

    if (flip) {
      code |= ONE << move;
    }
  }

  return {code ?: m_neighbour_moves};
}

Board::MoveIterator Board::end() const { return {ZERO}; }

int Board::random_move(UniformRandomGenerator& g) const {
  int moves[NXN];
  int count = 0;
  for (int move : *this) {
    moves[count++] = move;
  }
  return moves[g(count)];
}

void Board::do_move(int move) {
  const auto flag = ONE << move;
  int t = m_turns & 1;
  m_pieces[t] |= flag;
  m_neighbour_moves &= ~flag;
  m_filled |= flag;
  for_each(MOVE_ITERATORS[move], MOVE_ITERATORS[move] + ITERATORS_COUNT,
           [this](const vector<int>& fields) {
             auto start = fields.begin(), stop = start;
             for (auto it = start; it != fields.end() and filled(*it); ++it) {
               if (owns(*it)) {
                 stop = it;
               }
             }

             for_each(start, stop, [this](int i) { flip(i); });
           });

  for_each(NEIGHBOURS[move].begin(), NEIGHBOURS[move].end(), [this](int n) {
    if (not filled(n)) {
      m_neighbour_moves |= ONE << n;
    }
  });

  ++m_turns;
  m_last_move = move;
}

bool Board::end_game() const { return m_turns == MAX_TURNS; }

int Board::score() const {
  int w = 0;
  int b = 0;

  for (auto i = 0u; i < NXN; ++i) {
    if (m_pieces[0] & (ONE << i)) {
      ++w;
    } else {
      ++b;
    }
  }

  return (w - b);
}

int Board::turns() const { return m_turns; }

void Board::flip(int i) {
  const auto flag = ONE << i;
  m_pieces[0] ^= flag;
  m_pieces[1] ^= flag;
}

bool Board::filled(int i) const { return m_filled & (ONE << i); }

bool Board::owns(int i) const { return m_pieces[m_turns & 1] & (ONE << i); }
