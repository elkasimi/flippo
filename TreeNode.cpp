#include "TreeNode.h"
#include "Helpers.h"

#include <algorithm>

using namespace std;

TreeNodeAllocator::TreeNodeAllocator() : nodes(32768), index(-1) {}

TreeNode *TreeNodeAllocator::operator()() {
  do {
    ++index;
    index &= 32767;
  } while (not nodes[index].available);
  return &nodes[index];
}

TreeNode::TreeNode() : available(true) {}

void TreeNode::dispose() { available = true; }

void TreeNode::dispose_all() {
  available = true;
  for (auto child = first_child; child; child = child->next) {
    child->dispose_all();
  }
}

TreeNode::TreeNode(TreeNode *parent, const Board &board)
    : parent(parent),
      first_child(nullptr),
      next(nullptr),
      player(board.last_moved()),
      move(board.last_move()),
      untried_count(0),
      value(0.0),
      visits(0),
      available(false) {
  for (int move : board) {
    untried_moves[untried_count++] = move;
  }
}

TreeNode *TreeNode::expand(TreeNodeAllocator &allocator, Board &board) {
  int a = untried_moves[0];
  untried_moves[0] = untried_moves[--untried_count];
  board.do_move(a);
  auto child = make_tree_node(allocator, this, board);
  child->next = first_child;
  first_child = child;
  return first_child;
}

TreeNode *TreeNode::select(UniformRandomGenerator &g) const {
  return g(5) == 0 ? select_randomly(g) : select_best();
}

TreeNode::Iterator::Iterator(TreeNode *node) : node(node) {}

void TreeNode::Iterator::operator++() {
  node = node->next;
  __builtin_prefetch(node, 0, 1);
}

TreeNode *TreeNode::Iterator::operator*() { return node; }

bool TreeNode::Iterator::operator!=(const TreeNode::Iterator &other) const {
  return node != other.node;
}

TreeNode::Iterator TreeNode::begin() const { return {first_child}; }

TreeNode::Iterator TreeNode::end() const { return {nullptr}; }

TreeNode *TreeNode::select_most_visited() const {
  TreeNode *most_visited = nullptr;
  int max_visits = 0;
  for (auto child : *this) {
    if (max_visits < child->visits) {
      most_visited = child;
      max_visits = child->visits;
    }
  }

  return most_visited;
}

TreeNode *TreeNode::select_best() const {
  TreeNode *best_child = nullptr;
  double best_value = numeric_limits<double>::lowest();
  for (auto child : *this) {
    if (best_value < child->value) {
      best_value = child->value;
      best_child = child;
    }
  }

  return best_child;
}

TreeNode *TreeNode::select_randomly(UniformRandomGenerator &g) const {
  TreeNode *children[NXN];
  int count = 0;
  for (auto child : *this) {
    children[count++] = child;
  }
  return children[g(count)];
}

bool TreeNode::leaf() const { return untried_count == 0 and not first_child; }

bool TreeNode::fully_expanded() const {
  return untried_count == 0 and first_child;
}

void TreeNode::update(const int &payoff) {
  double v = player == WHITE ? payoff : -payoff;
  value = value * visits + v;
  value /= ++visits;
}

TreeNode *make_root(TreeNodeAllocator &allocator, const Board &board) {
  return make_tree_node(allocator, nullptr, board);
}

TreeNode *make_tree_node(TreeNodeAllocator &allocator, TreeNode *parent,
                         const Board &board) {
  auto node = allocator();
  new (node) TreeNode(parent, board);
  return node;
}
