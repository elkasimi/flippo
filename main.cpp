#include "Board.h"
#include "Helpers.h"
#include "MCTS.h"

int main() {
  Board board{};
  MCTS ai{board};
  IOStream stream;
  while (stream.get() and not stream.quit()) {
    if (not stream.start()) {
      ai.play_move(stream.move());
    }

    stream.print(ai.play_best_move());
  }

  return 0;
}
