#pragma once

#include <cstdint>

struct UniformRandomGenerator;

constexpr int N{8};
constexpr int NXN{64};
constexpr int MAX_TURNS{60};

constexpr int WHITE{0};
constexpr int BLACK{1};

class Board {
 public:
  struct MoveIterator {
    MoveIterator(uint64_t c);

    void operator++();
    int operator*();
    bool operator!=(const MoveIterator& other) const;

    uint64_t code;
  };

  Board();

  int last_moved() const;
  int last_move() const;
  MoveIterator begin() const;
  MoveIterator end() const;
  int random_move(UniformRandomGenerator& g) const;
  void do_move(int move);
  bool end_game() const;
  int score() const;
  int turns() const;

 private:
  void flip(int i);
  bool filled(int i) const;
  bool owns(int i) const;

  uint64_t m_pieces[2];
  uint64_t m_neighbour_moves;
  uint64_t m_filled;
  int m_turns;
  int m_last_move;
};
