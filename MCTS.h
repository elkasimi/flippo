#pragma once

#include "Helpers.h"
#include "TreeNode.h"

#include <vector>

class Board;
struct TreeNode;

class MCTS {
 public:
  MCTS(Board& board);

  void set_max_total_time(int max_time_ms);
  void set_max_iterations(int max_iterations);

  void play_move(int move);
  int play_best_move();
  void disable_opening_book();

 private:
  int get_max_time();
  int playout(Board& board) const;
  int best_move();
  void change_root(int move);

  Board& m_board;
  TreeNode* m_root;
  int m_max_total_time;
  int m_total_time;
  int m_max_iterations;
  std::vector<int> m_moves;
  mutable UniformRandomGenerator m_generator;
  mutable TreeNodeAllocator m_allocator;
  bool m_use_opening_book;
};
